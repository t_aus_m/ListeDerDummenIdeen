EESchema Schematic File Version 4
LIBS:platine-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ESP8266:NodeMCU1.0(ESP-12E) U1
U 1 1 5BEAD11C
P 1950 2100
F 0 "U1" H 1950 3187 60  0000 C CNN
F 1 "NodeMCU1.0(ESP-12E)" H 1950 3081 60  0000 C CNN
F 2 "ESP8266:NodeMCU-LoLinV3" H 1350 1250 60  0000 C CNN
F 3 "" H 1350 1250 60  0000 C CNN
	1    1950 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J2
U 1 1 5BEAD2A2
P 3800 1950
F 0 "J2" H 3880 1992 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 3880 1901 50  0000 L CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00057_1x03_P5.00mm_45Degree" H 3800 1950 50  0001 C CNN
F 3 "~" H 3800 1950 50  0001 C CNN
	1    3800 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J3
U 1 1 5BEAD2E6
P 3800 2350
F 0 "J3" H 3880 2392 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 3880 2301 50  0000 L CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00057_1x03_P5.00mm_45Degree" H 3800 2350 50  0001 C CNN
F 3 "~" H 3800 2350 50  0001 C CNN
	1    3800 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5BEAD3E0
P 3800 2700
F 0 "J4" H 3880 2692 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 3880 2601 50  0000 L CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00056_1x02_P5.00mm_45Degree" H 3800 2700 50  0001 C CNN
F 3 "~" H 3800 2700 50  0001 C CNN
	1    3800 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Counter_Clockwise J1
U 1 1 5BEAD69D
P 2150 4000
F 0 "J1" H 2200 4517 50  0000 C CNN
F 1 "Conn_02x08_Counter_Clockwise" H 2200 4426 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x08_P2.54mm_Vertical" H 2150 4000 50  0001 C CNN
F 3 "~" H 2150 4000 50  0001 C CNN
	1    2150 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5BEADAE7
P 4150 3150
F 0 "#PWR04" H 4150 3000 50  0001 C CNN
F 1 "+5V" V 4165 3278 50  0000 L CNN
F 2 "" H 4150 3150 50  0001 C CNN
F 3 "" H 4150 3150 50  0001 C CNN
	1    4150 3150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5BEADB42
P 4150 3300
F 0 "#PWR05" H 4150 3050 50  0001 C CNN
F 1 "GND" V 4155 3172 50  0000 R CNN
F 2 "" H 4150 3300 50  0001 C CNN
F 3 "" H 4150 3300 50  0001 C CNN
	1    4150 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 3150 3400 3150
Wire Wire Line
	3400 3150 3400 2700
Wire Wire Line
	3400 2700 3600 2700
Wire Wire Line
	4150 3300 3600 3300
Wire Wire Line
	3600 3300 3600 2800
Wire Wire Line
	3400 2700 3100 2700
Wire Wire Line
	3100 2700 3100 3150
Wire Wire Line
	3100 3150 1150 3150
Wire Wire Line
	1150 3150 1150 2800
Connection ~ 3400 2700
Wire Wire Line
	3600 2800 3200 2800
Wire Wire Line
	3200 2800 3200 3250
Wire Wire Line
	3200 3250 3000 3250
Wire Wire Line
	1050 3250 1050 2700
Wire Wire Line
	1050 2700 1150 2700
Connection ~ 3600 2800
$Comp
L power:Earth #PWR03
U 1 1 5BEAE250
P 4100 1650
F 0 "#PWR03" H 4100 1400 50  0001 C CNN
F 1 "Earth" H 4100 1500 50  0001 C CNN
F 2 "" H 4100 1650 50  0001 C CNN
F 3 "~" H 4100 1650 50  0001 C CNN
	1    4100 1650
	0    -1   -1   0   
$EndComp
$Comp
L power:LINE #PWR01
U 1 1 5BEAE81C
P 4100 1450
F 0 "#PWR01" H 4100 1300 50  0001 C CNN
F 1 "LINE" V 4117 1578 50  0000 L CNN
F 2 "" H 4100 1450 50  0001 C CNN
F 3 "" H 4100 1450 50  0001 C CNN
	1    4100 1450
	0    1    1    0   
$EndComp
$Comp
L power:NEUT #PWR02
U 1 1 5BEAEA14
P 4100 1550
F 0 "#PWR02" H 4100 1400 50  0001 C CNN
F 1 "NEUT" V 4117 1678 50  0000 L CNN
F 2 "" H 4100 1550 50  0001 C CNN
F 3 "" H 4100 1550 50  0001 C CNN
	1    4100 1550
	0    1    1    0   
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5BEAF022
P 3250 2000
F 0 "F1" H 3310 2046 50  0000 L CNN
F 1 "Fuse" H 3310 1955 50  0000 L CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" V 3180 2000 50  0001 C CNN
F 3 "~" H 3250 2000 50  0001 C CNN
	1    3250 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 1950 3550 1950
Wire Wire Line
	3550 1950 3550 2350
Wire Wire Line
	3550 2350 3600 2350
Wire Wire Line
	3600 2050 3500 2050
Wire Wire Line
	3500 2050 3500 2450
Wire Wire Line
	3500 2450 3600 2450
Wire Wire Line
	3600 1850 3450 1850
Wire Wire Line
	3250 2150 3250 2250
Wire Wire Line
	3250 2250 3600 2250
Wire Wire Line
	3450 1450 3450 1850
Wire Wire Line
	3450 1450 4100 1450
Connection ~ 3450 1850
Wire Wire Line
	3450 1850 3250 1850
Wire Wire Line
	4100 1550 3550 1550
Wire Wire Line
	3550 1550 3550 1950
Connection ~ 3550 1950
Wire Wire Line
	4100 1650 3500 1650
Wire Wire Line
	3500 1650 3500 2050
Connection ~ 3500 2050
Text Label 2450 3700 0    50   ~ 0
G1
Text Label 1950 3700 2    50   ~ 0
R1
Text Label 1950 3800 2    50   ~ 0
B1
Text Label 1950 3900 2    50   ~ 0
R2
Text Label 1950 4000 2    50   ~ 0
B2
Text Label 1950 4100 2    50   ~ 0
A
Text Label 1950 4200 2    50   ~ 0
C
Text Label 1950 4300 2    50   ~ 0
CLK
Text Label 1950 4400 2    50   ~ 0
OE
Text Label 2450 3800 0    50   ~ 0
GND
Text Label 2450 4000 0    50   ~ 0
GND
Text Label 2450 4400 0    50   ~ 0
GND
Text Label 2450 3900 0    50   ~ 0
G2
Text Label 2450 4100 0    50   ~ 0
B
Text Label 2450 4200 0    50   ~ 0
D
Text Label 2450 4300 0    50   ~ 0
LAT
$Comp
L Connector:Conn_01x04_Female J5
U 1 1 5BEB0879
P 3800 3550
F 0 "J5" H 3827 3526 50  0000 L CNN
F 1 "Conn_01x04_Female" H 3827 3435 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3800 3550 50  0001 C CNN
F 3 "~" H 3800 3550 50  0001 C CNN
	1    3800 3550
	1    0    0    -1  
$EndComp
Text Label 3600 3450 2    50   ~ 0
VCC
Text Label 3600 3550 2    50   ~ 0
VCC
Text Label 3600 3650 2    50   ~ 0
GND
Text Label 3600 3750 2    50   ~ 0
GND
Wire Wire Line
	3100 3150 3100 3450
Wire Wire Line
	3100 3450 3600 3450
Connection ~ 3100 3150
Wire Wire Line
	3100 3450 3100 3550
Wire Wire Line
	3100 3550 3600 3550
Connection ~ 3100 3450
Wire Wire Line
	3000 3250 3000 3650
Wire Wire Line
	3000 3650 3600 3650
Connection ~ 3000 3250
Wire Wire Line
	3000 3250 1050 3250
Wire Wire Line
	3000 3650 3000 3750
Wire Wire Line
	3000 3750 3600 3750
Connection ~ 3000 3650
Wire Wire Line
	2450 3800 3000 3800
Wire Wire Line
	3000 3800 3000 3750
Connection ~ 3000 3750
Wire Wire Line
	2450 4000 3000 4000
Wire Wire Line
	3000 4000 3000 3800
Connection ~ 3000 3800
Wire Wire Line
	2450 4400 3000 4400
Wire Wire Line
	3000 4400 3000 4000
Connection ~ 3000 4000
Text GLabel 1700 3700 0    50   Output ~ 0
R1
Text GLabel 1700 3800 0    50   Output ~ 0
B1
Text GLabel 2700 3700 2    50   Output ~ 0
G1
Text GLabel 2750 1700 2    50   Input ~ 0
G1
Text GLabel 2750 1600 2    50   Input ~ 0
R1
Text GLabel 2750 1800 2    50   Input ~ 0
B1
Wire Wire Line
	1700 3700 1950 3700
Wire Wire Line
	1950 3800 1700 3800
Wire Wire Line
	2700 3700 2450 3700
Text GLabel 1700 3900 0    50   Output ~ 0
R2
Text GLabel 2750 2100 2    50   Input ~ 0
R2
Text GLabel 2700 3900 2    50   Output ~ 0
G2
Text GLabel 2750 2200 2    50   Input ~ 0
G2
Text GLabel 1700 4000 0    50   Output ~ 0
B2
Text GLabel 2750 2300 2    50   Input ~ 0
B2
Wire Wire Line
	1700 3900 1950 3900
Wire Wire Line
	1700 4000 1950 4000
Wire Wire Line
	2700 3900 2450 3900
Text GLabel 1700 4100 0    50   Output ~ 0
A
Text GLabel 1700 4200 0    50   Output ~ 0
C
Text GLabel 2700 4100 2    50   Output ~ 0
B
Text GLabel 2700 4200 2    50   Output ~ 0
D
Text GLabel 1150 1700 0    50   Input ~ 0
A
Text GLabel 1150 1800 0    50   Input ~ 0
B
Text GLabel 2750 1400 2    50   Input ~ 0
C
Text GLabel 2750 1500 2    50   Input ~ 0
D
Wire Wire Line
	1700 4100 1950 4100
Wire Wire Line
	1700 4200 1950 4200
Wire Wire Line
	2700 4100 2450 4100
Wire Wire Line
	2700 4200 2450 4200
Text GLabel 1700 4300 0    50   Output ~ 0
CLK
Text GLabel 1700 4400 0    50   Output ~ 0
OE
Text GLabel 2700 4300 2    50   Output ~ 0
LAT
Wire Wire Line
	1700 4300 1950 4300
Wire Wire Line
	1700 4400 1950 4400
Wire Wire Line
	2450 4300 2700 4300
Text GLabel 2750 2400 2    50   Input ~ 0
CLK
Text GLabel 2750 2500 2    50   Input ~ 0
OE
Text GLabel 2750 2600 2    50   Input ~ 0
LAT
$EndSCHEMATC
