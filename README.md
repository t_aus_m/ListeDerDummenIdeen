# ListeDerDummenIdeen

*Eine Liste mit dummen, aber teilsweise auch sinnvollen Ideen für Hardware-Projekte von @zottelchin und @t_aus_m*

Details zu Projekten werden als Unterordner angelegt und der Einfachheit halber in der Readme.md verlinkt

- [x] [Eine Liste mit dummen Ideen als GitHub-Repo anlegen](https://codeberg.org/t_aus_m/ListeDerDummenIdeen)
- [ ] [Timecode Displays, über WLAN synchronisiert](https://codeberg.org/t_aus_m/ListeDerDummenIdeen/blob/master/TimeCode/Recherche.md)
- [ ] [Flaschenpost mit GPS-Tracking](https://codeberg.org/t_aus_m/ListeDerDummenIdeen/blob/master/GPSFlasche/Recherche.md)
- [ ] Rückfahrkamera für den Anhänger
- [ ] Presenter selber bauen
- [ ] Anhänger-Beleuchtungs-Tester bauen
- [ ] Tilix-fork erstellen und umbenennen (in TillX)
